class Api {
    constructor() {
        this.url = 'https://api.themoviedb.org/3/movie/popular?api_key=9ab4e0c0c4d3ba62a8ae20bc1aaa38f1';
        this.headers = { "Content-Type": "application/json" };
    }

    async fetchPopularMovies() {
        try {
            const res = await fetch(this.url, {
                headers: this.headers
            });

            const data = await res.json();
            return data.results;
        } catch (err) {
            console.log("Error ", err);
        }
    }
}

const renderPopularMovies = async () => {
    const loading = document.getElementById("load");
    try {
        const filmsFetch = new Api();
        const fetchedFilms = await filmsFetch.fetchPopularMovies();
        fetchedFilms.map(film => {

            const listof = document.querySelector('ul');
            const popularFilm = document.createElement('li');

            popularFilm.innerHTML = `
                <p>${film.original_title}</p>
                <img src="https://www.themoviedb.org/t/p/w200/${film.poster_path}" alt="${film.overview}">
                <a href="#" class="like-button">
                    <i class="fas fa-heart"></i>
                </a>
                `;
            listof.append(popularFilm);
        })

        const likeButtons = document.querySelectorAll('a.like-button');
        likeButtons.forEach((buttonElement) => {
            buttonElement.addEventListener('click', (evt) => {
                evt.preventDefault();
                buttonElement.classList.toggle('like-button-active');
            })
        });

    } catch (err) {
        console.log(err);
    } finally {
        loading.remove();
    }
}

renderPopularMovies();